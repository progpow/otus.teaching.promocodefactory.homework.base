﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public static class BaseRepositoryExtension<T> where T:BaseEntity
    {
        public static void CheckEnitiyIsNotNull(T entity)
        {
            if(entity == null)
                throw new InvalidRepositoryRequestException($"Передана сущность '{typeof(T)}' " +
                                                            $"со значением null");
        }
        
        public static void CheckIdExists(Guid id)
        {
            if(id == Guid.Empty)
                throw new InvalidRepositoryRequestException($"Не заполнен идентификатор " +
                                                            $"в сущности '{typeof(T)}'");
        }

        public static void CheckIdNotExists(Guid id, IEnumerable<T>  data)
        {
            if(id != Guid.Empty)
                if(data.Any(p=>p.Id == id))
                    throw new InvalidRepositoryRequestException($"Сущность '{typeof(T)}' " +
                                                                $"с идентификатором {id} уже существует");
        }
    }
}