﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using static Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.BaseEntityRepositoryExtension;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeesRepository: IRepository<Employee>
    {
        private List<Employee> Data { get; set; }
        private IRepository<Role> RolesRepository { get; set; }

        public EmployeesRepository(IRepository<Role> rolesRepository)
        {
            Data = new List<Employee>(FakeDataFactory.Employees);
            RolesRepository = rolesRepository;
        }
        
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            var result = Data.Select(async p =>
            {
                Employee employee = await UpdateRolesForEmployee(p);
                return employee;
            });
            List<Employee> ret = new List<Employee>();
            foreach (var item in result)
                ret.Add(await item);
            return ret;
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            return await UpdateRolesForEmployee(Data.FirstOrDefault(p=>p.Id == id));
        }

        public async Task InsertAsync(Employee entity)
        {
            await Task.Run(() =>
            {
                CheckInputEnitiyIsNotNull(entity);
                GenerateId(entity);
                CheckIdNotExists(entity, Data);
                Data.Add(entity);
            });
        }

        public async Task UpdateAsync(Employee updatedEntity)
        {
            CheckInputEnitiyIsNotNull(updatedEntity);
            CheckIdExists(updatedEntity, Data);
            var updatingEntity = await GetByIdAsync(updatedEntity.Id);
            Data.Remove(updatingEntity);
            Data.Add(updatedEntity);    
        }

        public async Task DeleteAsync(Guid id)
        {
            CheckIdExists(id);
            var deletingEntry = await GetByIdAsync(id);
            CheckIdExists(deletingEntry, Data);
            Data.Remove(deletingEntry);
        }
        
        private async Task<Employee> UpdateRolesForEmployee(Employee employee)
        {
            if (employee.Roles == null)
                return employee;
            var dynamicRoles = new List<Role>();
            
            foreach (var employeeRole in employee.Roles)
            {
                var role = await RolesRepository.GetByIdAsync(employeeRole.Id);
                if (role != null)
                    dynamicRoles.Add(role);
            }

            employee.Roles = dynamicRoles;
            return employee;
        }

    }
}