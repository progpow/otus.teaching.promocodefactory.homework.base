﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public static class BaseEntityRepositoryExtension
    {
        public static void CheckInputEnitiyIsNotNull<T>(T entity) where T:BaseEntity
        {
            if(entity == null)
                throw new InvalidRepositoryRequestException($"Передана сущность '{typeof(T)}' " +
                                                            $"со значением null");
        }

        public static void CheckIdExists(Guid id)
        {
            if(id == Guid.Empty)
                throw new InvalidRepositoryRequestException($"Не заполнен идентификатор");
        }
        
        public static void CheckInputEntityIdValid<T>(T entity) where T:BaseEntity
        {
            if(entity.Id == Guid.Empty)
                throw new InvalidRepositoryRequestException($"Не заполнен идентификатор " +
                                                            $"в сущности '{typeof(T)}'");
        }

        public static void CheckIdNotExists<T>(T entity, IEnumerable<T>  data) where T:BaseEntity
        {
            if(entity.Id != Guid.Empty)
                if(data.Any(p=>p.Id == entity.Id))
                    throw new InvalidRepositoryRequestException($"Сущность '{typeof(T)}' " +
                                                                $"с идентификатором {entity.Id} уже существует");
        }
        
        public static void GenerateId<T>(T entity) where T:BaseEntity
        {
            if (entity.Id != Guid.Empty)
                throw new Exception("Попытка сгенерировать Id для сущности у которой уже есть идентификатор.");
            entity.Id = Guid.NewGuid();
        }

        public static void CheckIdExists<T>(T entity, IEnumerable<T> data) where T:BaseEntity
        {
            CheckIdExists(entity.Id, data);
        }
        
        public static void CheckIdExists<T>(Guid id, IEnumerable<T> data) where T:BaseEntity
        {
            if (id != Guid.Empty)
                if (data.All(p => p.Id != id))
                    throw new InvalidRepositoryRequestException($"Сущность '{typeof(T)}' " +
                                                                $"с идентификатором {id} не существует");
        }
    }
}