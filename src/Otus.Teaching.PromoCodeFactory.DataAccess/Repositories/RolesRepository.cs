﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using static Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.BaseEntityRepositoryExtension;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class RolesRepository: IRepository<Role>
    {
        private List<Role> Data { get; set; }

        public RolesRepository()
        {
            Data = new List<Role>(FakeDataFactory.Roles);
        }
        public Task<IEnumerable<Role>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<Role> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(p=>p.Id == id));
        }

        public async Task InsertAsync(Role entity)
        {
            await Task.Run(() =>
            {
                CheckInputEnitiyIsNotNull(entity);
                GenerateId(entity);
                Data.Add(entity);
            });
        }

        public async Task UpdateAsync(Role updatedEntity)
        {
            CheckInputEnitiyIsNotNull(updatedEntity);
            CheckInputEntityIdValid(updatedEntity);
            CheckIdExists(updatedEntity, Data);
            var updatingEntity = await GetByIdAsync(updatedEntity.Id);
            Data.Remove(updatingEntity);
            Data.Add(updatedEntity);
        }

        public async Task DeleteAsync(Guid id)
        {
            CheckIdExists(id);
            CheckIdExists(id, Data);
            var updatingEntity = await GetByIdAsync(id);
            Data.Remove(updatingEntity);
        }
    }
}