﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Exceptions
{
    public class EntityNotFoundException:Exception
    {
        public EntityNotFoundException(string message): base(message)
        {
            
        }
    }
}