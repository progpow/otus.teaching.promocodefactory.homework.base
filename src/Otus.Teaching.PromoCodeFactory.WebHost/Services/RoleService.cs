﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class RoleService : EntityService<Role, RoleInsertRequest, RoleUpdateRequest, RoleResponse, RoleResponse>
    {
        public RoleService(IRepository<Role> roleRepository): base(roleRepository)
        {
        }

        protected override RoleResponse ParseRepositoryEntityShort(Role role)
        {
            if (role == null)
                return null;
            return new RoleResponse()
            {
                Id = role.Id,
                Description = role.Description,
                Name = role.Name,
            };
        }

        protected override RoleResponse ParseRepositoryEntity(Role role)
        {
            return ParseRepositoryEntityShort(role);
        }

        protected override Role ParseRoleInsertRequest(RoleInsertRequest insertEntity)
        {
            return new  Role()
            {
                Description = insertEntity.Description,
                Name = insertEntity.Name
            };
        }

        protected override Role ParseRoleUpdateRequest(RoleUpdateRequest updatedEntity)
        {
            return new Role()
            {
                Id = updatedEntity.Id,
                Description = updatedEntity.Description,
                Name = updatedEntity.Name
            };
        }

    }
}