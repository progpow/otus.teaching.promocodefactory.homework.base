﻿using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class EmployeeService: EntityService<Employee,EmployeeInsertRequest, EmployeeUpdateRequest, EmployeeResponse, EmployeeShortResponse>
    {
        public EmployeeService(IRepository<Employee> employeeRepository): base(employeeRepository)
        {
            
        }

        protected override EmployeeShortResponse ParseRepositoryEntityShort(Employee employee)
        {
            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName
            };
        }

        protected override EmployeeResponse ParseRepositoryEntity(Employee employee)
        {
            return new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }

        protected override Employee ParseRoleInsertRequest(EmployeeInsertRequest insertEntity)
        {
            return new Employee()
            {
                Email = insertEntity.Email,
                FirstName = insertEntity.FirstName,
                LastName = insertEntity.LastName,
                Roles =insertEntity.Roles?.Select(p => new Role()
                {
                    Id = p
                }),
                AppliedPromocodesCount = insertEntity.AppliedPromocodesCount
            };
        }

        protected override Employee ParseRoleUpdateRequest(EmployeeUpdateRequest updatedEntity)
        {
            return new Employee()
            {
                Id = updatedEntity.Id,
                Email = updatedEntity.Email,
                FirstName = updatedEntity.FirstName,
                LastName = updatedEntity.LastName,
                Roles = updatedEntity.Roles?.Select(p => new Role()
                {
                    Id = p
                }),
                AppliedPromocodesCount = updatedEntity.AppliedPromocodesCount
            };
        }
    }
}