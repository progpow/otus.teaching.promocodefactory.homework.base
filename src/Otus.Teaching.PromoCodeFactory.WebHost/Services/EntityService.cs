﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.WebHost.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public abstract class
        EntityService<TRepositoryEntity, TInsertRequest, TUpdateRequest, TResponse, TShortResponse> 
        : IEntityService<TInsertRequest, TUpdateRequest, TResponse, TShortResponse>
        where TRepositoryEntity: BaseEntity 
        where TUpdateRequest: IUpdateRequest 
    {
        private readonly IRepository<TRepositoryEntity> _entityRepository;
        
        protected abstract TShortResponse ParseRepositoryEntityShort(TRepositoryEntity arg);
        protected abstract TResponse ParseRepositoryEntity(TRepositoryEntity role);
        protected abstract TRepositoryEntity ParseRoleInsertRequest(TInsertRequest insertEntity);
        protected abstract TRepositoryEntity ParseRoleUpdateRequest(TUpdateRequest updatedEntity);
        
        protected EntityService(IRepository<TRepositoryEntity> entityRepository)
        {
            _entityRepository = entityRepository;
        }
        public async Task<IEnumerable<TShortResponse>> GetAllAsync()
        {
            var repositoryEntities = await _entityRepository.GetAllAsync();
            var responseModelList = repositoryEntities.Select(ParseRepositoryEntityShort);
            return responseModelList;
        }
        
        public async Task<TResponse> GetByIdAsync(Guid id)
        {
            var repositoryEntity = await _entityRepository.GetByIdAsync(id);
            if (repositoryEntity == null)
                throw new EntityNotFoundException($"Невозможно найти сущность с идентификатором {id}");
            var entityModel = ParseRepositoryEntity(repositoryEntity);
            return entityModel;
        }

        public async Task InsertAsync(TInsertRequest insertEntity)
        {
            await _entityRepository.InsertAsync(ParseRoleInsertRequest(insertEntity));
        }

        public async Task UpdateAsync(TUpdateRequest updatedEntity)
        {
            var repositoryEntityUpdating = await _entityRepository.GetByIdAsync(updatedEntity.Id);

            if (repositoryEntityUpdating == null)
                throw new EntityNotFoundException(string.Empty);

            await _entityRepository.UpdateAsync(ParseRoleUpdateRequest(updatedEntity));
        }

        public async Task DeleteAsync(Guid id)
        {
            await _entityRepository.DeleteAsync(id);
        }
    }
}