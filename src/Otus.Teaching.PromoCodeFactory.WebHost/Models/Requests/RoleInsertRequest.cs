﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class RoleInsertRequest
    {
        public string Description { get; set; }
        public string Name { get; set; }
    }
}