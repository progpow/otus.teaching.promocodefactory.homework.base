﻿using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class RoleUpdateRequest : IUpdateRequest
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }
}