﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class EmployeeUpdateRequest: EmployeeInsertRequest, IUpdateRequest
    {
        public Guid Id { get; set; }
    }
}