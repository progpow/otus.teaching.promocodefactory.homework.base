﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests
{
    public class EmployeeInsertRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Guid[] Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}