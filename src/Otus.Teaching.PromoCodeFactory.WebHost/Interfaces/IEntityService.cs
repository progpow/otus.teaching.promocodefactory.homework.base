﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Interfaces
{
    public interface IEntityService<in TInsertRequest,in TUpdateRequest, TResponse, TShortResponse>
    {
        Task<IEnumerable<TShortResponse>> GetAllAsync();
        
        Task<TResponse> GetByIdAsync(Guid id);

        Task InsertAsync(TInsertRequest entity);

        Task UpdateAsync(TUpdateRequest updatedEntity);

        Task DeleteAsync(Guid id);
    }
}