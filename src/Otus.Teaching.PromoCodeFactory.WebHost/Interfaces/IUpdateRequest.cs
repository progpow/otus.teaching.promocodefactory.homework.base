﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Interfaces
{
    public interface IUpdateRequest
    {
        public Guid Id { get; set; }
    }
}