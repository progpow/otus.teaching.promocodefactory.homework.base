﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEntityService<EmployeeInsertRequest, EmployeeUpdateRequest, EmployeeResponse, EmployeeShortResponse> _employeeService;
        public EmployeesController(IEntityService<EmployeeInsertRequest, EmployeeUpdateRequest, EmployeeResponse, EmployeeShortResponse> employeeService)
        {
            _employeeService = employeeService;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<EmployeeShortResponse>> GetEmployeesAsync()
        {
            return await _employeeService.GetAllAsync();
        }
        
        /// <summary>
        /// Получить данные сотрудника по Идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Результат операции</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeService.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            return employee;
        }

        /// <summary>
        /// Удалить данные сотрудника по Идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            try
            {
                var employee = await _employeeService.GetByIdAsync(id);
                await _employeeService.DeleteAsync(id);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            return Ok();
        }
        
        /// <summary>
        /// Обновление данных о сотруднике
        /// </summary>
        /// <param name="employeeUpdateRequest">Обновлённые данные по существующему сотруднику</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest employeeUpdateRequest)
        {
            try
            {
                await _employeeService.UpdateAsync(employeeUpdateRequest);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            return Ok();
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <param name="employeeInsertRequest">Данные о новом сотруднике</param>
        /// <returns>Результат выполения операции</returns>
        [HttpPost]
        public async Task<IActionResult> InsertEmployeeAsync(EmployeeInsertRequest employeeInsertRequest)
        {
            try
            {
                await _employeeService.InsertAsync(employeeInsertRequest);
            }
            catch (InvalidRepositoryRequestException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}