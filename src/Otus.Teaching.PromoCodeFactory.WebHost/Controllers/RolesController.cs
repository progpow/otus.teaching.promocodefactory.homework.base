﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Exceptions;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController: ControllerBase
    {
        private readonly IEntityService<RoleInsertRequest, RoleUpdateRequest, RoleResponse, RoleResponse> _rolesService;

        public RolesController(IEntityService<RoleInsertRequest, RoleUpdateRequest, RoleResponse, RoleResponse> rolesService)
        {
            _rolesService = rolesService;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleResponse>> GetRolesAsync()
        {
            return await _rolesService.GetAllAsync();
        }
        
        /// <summary>
        /// Удалить роль по Id
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <returns>Резудьтат операции</returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteRoleAsync(Guid id)
        {
            var role = await _rolesService.GetByIdAsync(id);

            if (role == null)
                return NotFound();

            await _rolesService.DeleteAsync(id);
            return Ok();
        }
        /// <summary>
        ///  Обновить данные роли
        /// </summary>
        /// <param name="roleUpdate">Обновлённая роль</param>
        /// <returns>Результат операции</returns>
        [HttpPut]
        public async Task<IActionResult> UpdateRoleAsync(RoleUpdateRequest roleUpdate)
        {
            var roleUpdating = await _rolesService.GetByIdAsync(roleUpdate.Id);

            if (roleUpdating == null)
                return NotFound();

            await _rolesService.UpdateAsync(roleUpdate);
            return Ok();
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="roleInsert">Роль для вставки</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        public async Task<IActionResult> InsertRoleAsync(RoleInsertRequest roleInsert)
        {
            try
            {
                await _rolesService.InsertAsync(roleInsert);
            }
            catch (InvalidRepositoryRequestException)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}