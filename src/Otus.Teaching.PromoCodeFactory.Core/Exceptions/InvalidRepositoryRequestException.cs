﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Exceptions
{
    /// <summary>
    /// Throw when we catch invalid request on  Repository layer
    /// </summary>
    public class InvalidRepositoryRequestException: Exception
    {
        public InvalidRepositoryRequestException(string message): base(message)
        {
            
        }
    }
}